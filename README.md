# README #

### What is this repository for? ###

* picoVersat is a 16-instruction picocontroller used to replace error prone FSMs 
* Version 0.0


### How do I get set up? ###

* Install Python (tested in 2.7)
* Install Icarus Verilog (download a stable version from http://iverilog.icarus.com)
* Install the picoVersat assembler 

```
    cd tools
    make [install]
```

* How to run tests

``` bash
cd tests
make {install|testname}
```

### Contribution guidelines ###

* Writing tests

[to be added]


### Who do I talk to? ###

* Repo owner or admin
