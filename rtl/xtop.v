`timescale 1ns / 1ps

`include "xdefs.vh"
`include "xctrldefs.vh"
`include "xdefs.vh"
`include "xregfdefs.vh"
`include "xledsdefs.vh"
`include "xuart_wrapperdefs.vh"

module xtop (
	     //INSERT EXTERNAL INTERFACES HERE

             // parallel interface
	     input  [`REGF_ADDR_W-1:0]    par_addr,
	     input                        par_we,
	     input  [`DATA_W-1:0]         par_in,
	     output [`DATA_W-1:0]         par_out,

`ifdef LEDS
             // output wires to connect to the FPGA LED pins
             output [`NUMBER_OF_LEDS-1:0] leds_out,
`endif

`ifdef UART
             // UART output wires
             output                       uart_tx_output,
             //input                        uart_rx_input,
`endif
             // MANDATORY INTERFACE SIGNALS
	     input 		          clk,
	     input 		          rst
	     );

   /****************************
    *                          *
    *     CONNECTION WIRES     *
    *                          *
    ****************************/
   
   // PROGRAM MEMORY/CONTROLLER INTERFACE
   wire [`INSTR_W-1:0]     instruction;
   wire [`PROG_ADDR_W-1:0] pc;

   // DATA BUS
   wire                    data_sel;
   wire                    data_we;
   wire [`ADDR_W-1:0]      data_addr;
   reg  [`DATA_W-1:0]      data_to_rd;
   wire [`DATA_W-1:0]      data_to_wr;

   // MODULE SELECTION SIGNALS
   reg                     prog_sel;
   wire [`DATA_W-1:0]      prog_data_to_rd;
   
   reg                     regf_sel;
   wire [`DATA_W-1:0]      regf_data_to_rd;

`ifdef DEBUG
   // PRINTER TO TERMINAL
   reg                     cprt_sel;
`endif

`ifdef LEDS
   // LEDS PERIPHERAL
   reg                     leds_sel;
`endif

`ifdef UART
   // SIMPLE UART
   wire                    uart_tx;
   wire                    uart_rx;
   reg                     uart_sel;
   reg [`DATA_W-1:0]       uart_data_to_rd;
   assign uart_tx_output     = uart_tx;
//   assign uart_rx_input     = uart_rx;
`endif

   /****************************
    *                          *
    *     FIXED SUBMODULES     *
    *                          *
    ****************************/
   
   /*****************************
    *     CONTROLLER MODULE     *
    *****************************/
   xctrl controller (
		     .clk(clk), 
		     .rst(rst),
		     
		     // Program memory interface
		     .pc(pc),
		     .instruction(instruction),
		     
		     // Data bus
		     .data_sel(data_sel),
		     .data_we (data_we), 
		     .data_addr(data_addr),
		     .data_to_rd(data_to_rd), 
		     .data_to_wr(data_to_wr)
		     );

   /*********************************
    *     PROGRAM MEMORY MODULE     *
    *********************************/
   xprog prog (
	       .clk(clk),

	       //data interface 
	       .data_sel(prog_sel),
	       .data_we(data_we),
	       .data_addr(data_addr[`PROG_RAM_ADDR_W-1:0]),
	       .data_in(data_to_wr),
	       .data_out(prog_data_to_rd),

	       //DMA interface 
`ifdef DMA_USE
	       .dma_req(dma_prog_req),	       
	       .dma_rnw(dma_rnw),
	       .dma_addr(dma_addr[`PROG_ADDR_W-1:0]),
	       .dma_data_in(dma_data_from),
	       .dma_data_out(dma_data_from_prog),
`endif	       

	       // instruction interface
	       .pc(pc),
       	       .instruction(instruction)      
	       );


   /***************************
    *     ADDRESS DECODER     *
    ***************************/
   always @ * begin
   
      /****************************
       *     default settings     *
       ****************************/

      data_to_rd = `DATA_W'd0;

      // PROGRAM MEMORY
      prog_sel = 1'b0;

      // HOST-CONTROLLER SHARED REGISTER FILE
      regf_sel = 1'b0;
      
//`ifdef DEBUG
      // PRINTER TO TERMINAL
      cprt_sel = 1'b0;
//`endif

`ifdef LEDS
      // LEDS PERIPHERAL
      leds_sel = 1'b0;
`endif

`ifdef UART
      // LEDS PERIPHERAL
      uart_sel = 1'b0;
`endif

      /*****************************************
       *     peripheral mapping and control    *
       *****************************************/

      // PROGRAM MEMORY
      if (`PROG_BASE == (data_addr & ({`ADDR_W{1'b1}}<<`PROG_ADDR_W))) begin
         prog_sel = 1'b1;
         data_to_rd = prog_data_to_rd;
      end

      // HOST-CONTROLLER SHARED REGISTER FILE
      else if (`REGF_BASE == (data_addr & ({`ADDR_W{1'b1}}<<`REGF_ADDR_W))) begin
	 regf_sel = data_sel;
         data_to_rd = regf_data_to_rd;
      end

//`ifdef DEBUG
      // PRINTER TO TERMINAL
      else if (`CPRT_BASE == data_addr)
	 cprt_sel = data_sel;
//`endif

`ifdef LEDS
      // LEDS PERIPHERAL
      else if (`LEDS_BASE == data_addr)
	 leds_sel = data_sel;
`endif

`ifdef UART
      else if (`UART_BASE == (data_addr & ({`ADDR_W{1'b1}}<<`UART_ADDR_W))) begin
         uart_sel = data_sel;
         data_to_rd = uart_data_to_rd;
      end
`endif
     
      else if(data_sel === 1'b1)
         $display("Warning: unmapped controller issued data address %x at time %f", data_addr, $time);

   end // always @ *

   /***************************************
    *                                     *
    *     USER MODULES INSERTED BELOW     *
    *                                     *
    ***************************************/

   /************************************************
    *     HOST-CONTROLLER SHARED REGISTER FILE     *
    ************************************************/
   xregf regf (
	       .clk(clk),
	       
	       //host interface (external)
	       .ext_we(par_we),
	       .ext_addr(par_addr),
	       .ext_data_in(par_in),
	       .ext_data_out(par_out),
			
	       //versat interface (internal)
	       .int_sel(regf_sel),
	       .int_we(data_we),
	       .int_addr(data_addr[`REGF_ADDR_W-1:0]),
	       .int_data_in(data_to_wr),
	       .int_data_out(regf_data_to_rd)
	       );

`ifdef DEBUG
   /*******************************
    *     PRINTER TO TERMINAL     *
    *******************************/
   xcprint cprint (
		   .clk(clk),
		   .sel(cprt_sel),
		   .data_in(data_to_wr[7:0])
		   );
`endif

`ifdef LEDS
   /***************************
    *     LEDS PERIPHERAL     *
    ***************************/
   xleds leds (
		   .clk(clk),
		   .sel(leds_sel),
		   .data_in(data_to_wr[`NUMBER_OF_LEDS-1:0]),
		   .data_out(leds_out)
		   );
`endif

`ifdef UART
   /***********************
    *     SIMPLE UART     *
    ***********************/
   xuart_wrapper uart (
                   .clk(clk),
                   .rst(rst),

                   .ser_tx(uart_tx),
                   .ser_rx(uart_rx),

                   .sel(uart_sel),
                   .we(data_we),
                   .addr(data_addr[`UART_ADDR_W-1:0]),
                   .data_in(data_to_wr),
                   .data_out(uart_data_to_rd)
                   );
   assign uart_rx = uart_tx;
`endif

endmodule
